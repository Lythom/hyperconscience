package tools.grid;
import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import flash.events.MouseEvent;
import flash.geom.Point;
import tools.grid.GridObject;

/**
 * ...
 * @author Samuel Bouchet
 */
class Grid extends Control
{
	static public inline var CLICKED:String = "clicked";
	
	private var _cells:Array<Array<Cell>>;
	private var _lineCount:Int;
	private var _columnCount:Int;
	
	
	/**
	 *
	 * @param	rowCount				number of cells horizontally
	 * @param	lineCount				number of cells verticaly
	 * @param	perspectiveOffset	perspective effect difference between top and bottom line in px
	 */
	public function new(x:Int, y:Int, columnCount:Int, lineCount:Int)
	{
		super(x, y);
		
		this.lineCount = lineCount;
		this.columnCount = columnCount;
		_cells = new Array<Array<Cell>>();
		
		// populate cell array with new cells
		var pos:Point = new Point(0, 0);
		var c:Cell;
		for (x in 0...columnCount) {
			_cells[x] = new Array<Cell>();
			for (y in 0...lineCount) {
				c =  new Cell(this, x, y);
				_cells[x][y] = c;
			}
		}
	}
	
	/**
	 * remove every object from the cells
	 */
	public function clearCellObjects() {
		for (x in 0...columnCount) {
			for (y in 0...lineCount) {
				if (getCell(x, y).gridObject != null) {
					getCell(x, y).gridObject = null;
				}
			}
		}
	}
	
	public function getCellObjects():Array<Dynamic> {
		var gridObjects:Array<Dynamic> = new Array<Dynamic>();
		for (x in 0...columnCount) {
			for (y in 0...lineCount) {
				var c = getCell(x, y);
				if (c.gridObject != null) {
					gridObjects.push(c.gridObject);
				}
			}
		}
		return gridObjects;
	}

	/**
	 * Return a cell if any exists at x,y.
	 * Return null if cell not found.
	 */
	public function getCell(x:Int, y:Int):ICell {
		if (x >= 0 && x < _cells.length) {
			if (y >= 0 && y < _cells[x].length) {
				return _cells[x][y];
			}
		}
		return null;
	}
	
	/**
	 * Remove any cell GridObject having this Object
	 */
	public function removeGridObject(go:GridObject):Void {
		for (x in 0...columnCount) {
			for (y in 0...lineCount) {
				if (_cells[x][y].gridObject == go) {
					_cells[x][y].gridObject = null;
				}
			}
		}
	}
	
	/**
	 * Look for a grid Object matching the condition. return Null if none is found.
	 * @param	condition
	 */
	public function lookForGridObject(condition:GridObject->Bool):GridObject {
		for (x in 0...columnCount) {
			for (y in 0...lineCount) {
				if (condition(_cells[x][y].gridObject)) {
					return _cells[x][y].gridObject;
				}
			}
		}
		return null;
	}
	
	override public function added():Void
	{
		super.added();
		if (HXP.stage != null) HXP.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
	}
	
	override public function removed():Void 
	{
		super.removed();
		if(HXP.stage != null) HXP.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
	}
	
	// mouseclick event
	private function onMouseUp(e:MouseEvent = null)
	{
		if(!this.active || !Input.mouseReleased || HXP.scene != this.scene) return;
		if (Control.getControlUnderMouse() == this) {
			dispatchEvent(new ControlEvent(this, CLICKED));
		}
	}
	
	/* INTERFACE grid.IGridGraphic */
	public function getCellPosition(cell:ICell):Point
	{
		return cast(graphic, IGridGraphic).getCellPosition(cell);
	}
	
	public function getCellAt(p:Point):ICell
	{
		return cast(graphic, IGridGraphic).getCellAt(p);
	}
	
	public var cellWidth(get_cellWidth, null):Int;
	function get_cellWidth() 
	{
		return cast(graphic, IGridGraphic).cellWidth;
	}
	public var cellHeight(get_cellHeight, null):Int;
	
	function get_cellHeight() 
	{
		return cast(graphic, IGridGraphic).cellHeight;
	}
	
	override private function set_graphic(value:Graphic):Graphic
	{
		if (!Std.is(value, IGridGraphic)) {
			throw "graphic must be an IGridGraphic";
		}
		var gridGraphic:IGridGraphic = cast(value, IGridGraphic);
		width = gridGraphic.getWidth(); // Math.floor(cast(value, AxonometricGridGraphic).width * HXP.screen.scale);
		height = gridGraphic.getHeight();  // Math.floor(cast(value, AxonometricGridGraphic).height * HXP.screen.scale);
		
		return super.set_graphic(value);
	}

	/* GETTERS AND SETTERS */
	
	private function get_cells():Array<Array<Cell>>
	{
		return _cells;
	}
	
	private function set_cells(value:Array<Array<Cell>>):Array<Array<Cell>>
	{
		return _cells = value;
	}
	
	public var cells(get_cells, set_cells):Array<Array<Cell>>;
	
	private function get_lineCount():Int
	{
		return _lineCount;
	}
	
	private function set_lineCount(value:Int):Int
	{
		return _lineCount = value;
	}
	
	public var lineCount(get_lineCount, set_lineCount):Int;
	
	private function get_columnCount():Int
	{
		return _columnCount;
	}
	
	private function set_columnCount(value:Int):Int
	{
		return _columnCount = value;
	}
	
	public var columnCount(get_columnCount, set_columnCount):Int;
}