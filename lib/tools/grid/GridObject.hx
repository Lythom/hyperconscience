package tools.grid;
import com.haxepunk.gui.Control;

/**
 * ...
 * @author Lythom
 */

class GridObject extends Control
{

	private var _description:String;
	private var _cell:Cell;
	
	public function new()
	{
		super();
		_description = "";
	}

	private function get_description():String
	{
		return _description;
	}
	
	private function set_description(value:String):String
	{
		return _description = value;
	}
	
	public var description(get_description, set_description):String;
	
	private function get_cell():Cell 
	{
		return _cell;
	}
	
	private function set_cell(value:Cell):Cell 
	{
		return _cell = value;
	}
	
	public var cell(get_cell, set_cell):Cell;
	
}