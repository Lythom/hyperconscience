package tools.grid;
import com.haxepunk.Graphic;
import flash.geom.Point;

/**
 * ...
 * @author Lythom
 */

interface ICell
{
	public function upperCell():ICell;
	public function bottomCell():ICell;
	public function leftCell():ICell;
	public function rightCell():ICell;
	
	public function upperRightCell():ICell;
	public function upperLeftCell():ICell;
	public function bottomLeftCell():ICell;
	public function bottomRightCell():ICell;
	
	/**
	 * Return all cells sharing a face with this one.
	 * @return
	 */
	public function adjacentCells():Array<ICell>;
	/**
	 * Return all cells sharing a face or a corner with this one.
	 * @return
	 */
	public function neighbourCells():Array<ICell>;
	
	/**
	 * Grid using this cell
	 */
	public var grid(get_grid, set_grid):Grid;
	
	/**
	 * X position in the grid. Between 0 and grid.columnCount-1.
	 */
	public var x(get_x, set_x):Int;
	
	/**
	 * Y position in the grid. Between 0 and grid.lineCount-1.
	 */
	public var y(get_y, set_y):Int;
	
	/**
	 * Grid object placed on this cell.
	 */
	public var gridObject(get_gridObject, set_gridObject):GridObject;
}