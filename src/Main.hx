import hyperconscience.WelcomeWorld;
import com.gigglingcorpse.utils.Profiler;
import com.haxepunk.Engine;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.CheckBox;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.RadioButton;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import hyperconscience.WelcomeWorld;
import openfl.display.StageDisplayState;
import openfl.Lib;

class Main extends Engine
{
	private var profiler:Profiler;

	public static inline var kScreenWidth:Int = 768;
	public static inline var kScreenHeight:Int = 1024;
	public static inline var kFrameRate:Int = 60;
	public static inline var kClearColor:Int = 0x64483e;
	public static inline var kProjectName:String = "HyperConscience";

	public function new()
	{
		super(kScreenWidth, kScreenHeight, kFrameRate, true);
	}

	override public function init()
	{
#if debug
	#if flash
		if (flash.system.Capabilities.isDebugger)
	#end
		{
			HXP.console.enable();
			HXP.console.toggleKey = Key.P;
		}
#end

		initHaxepunkGui();

		HXP.screen.color = kClearColor;
		HXP.screen.scale = 1;
		HXP.scene = new WelcomeWorld();

		Input.define("heart", [Key.D]);
		Input.define("lung", [Key.S, Key.F]);

		Input.define("pharynx", [Key.U]);
		Input.define("stomach", [Key.J]);
		Input.define("intestin", [Key.N]);

		Input.define("fullscreen", [Key.F4, Key.F11]);

		profiler = null;
	}

	public static function main()
	{
		new Main();
	}

	override public function update()
	{
		if (Input.pressed(Key.R)) {
			if (profiler == null) {
				profiler = new Profiler();
				HXP.stage.addChild(profiler);
				profiler.visible = true;
			} else {
				HXP.stage.removeChild(profiler);
				profiler = null;
			}
		}

		if (Input.pressed("fullscreen")) {
			gotoFullScreen();
		}
		super.update();
	}

	function gotoFullScreen()
	{

		if (Lib.current.stage.displayState == StageDisplayState.FULL_SCREEN_INTERACTIVE) {
			Lib.current.stage.displayState = StageDisplayState.NORMAL;
			HXP.screen.x = 0;
			HXP.screen.y = 0;
			HXP.screen.scaleX = 1;
			HXP.screen.scaleY = 1;
		} else {
			Lib.current.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;

			HXP.screen.scaleX = 1;
			HXP.screen.scaleY = 1;

			HXP.screen.x = Math.round(250);
			HXP.screen.y = 0;

		}
	}


	private function initHaxepunkGui():Void
	{
		// Choose custom skin. Parameter can be a String to resource or a bitmapData.
		Control.useSkin("gfx/ui/greyDefault.png");
		// The default layer where every component will be displayed on.
		// Most components use severals layers (at least 1 per component child). A child component layer will be <100.
		Control.defaultLayer = 100;
		// Use this to fit your button skin's borders, set the default padding of every new Button and ToggleButton.
		// padding attribute can be changed on instances after creation.
		Button.defaultPadding = 4;
		// Size in px of the tickBox for CheckBoxes. Default is skin native size : 12.
		CheckBox.defaultBoxSize = 12;
		// Same for RadioButtons.
		RadioButton.defaultBoxSize = 12;
		// Label defaults parameters affect every components that uses labels : Button, ToggleButton, CheckBox, RadioButton, MenuItem, Window Title.
		// Those labels are always accessible using "myComponent.label" and you can change specific Labels apperence any time.
		// Label default font (must be a flash.text.Font object).
		Label.defaultFont = openfl.Assets.getFont("font/pf_ronda_seven.ttf");
		// Label defaultColor. Tip inFlashDevelop : use ctrl + shift + k to pick a color.
		Label.defaultColor = 0x112C48;
		// Label default Size.
		Label.defaultSize = 32;
	}

}