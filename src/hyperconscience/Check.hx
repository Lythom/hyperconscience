package hyperconscience;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.Mask;
import openfl.Assets;

/**
 * ...
 * @author Lythom
 */
class Check extends Entity
{
	var spriteMap:Spritemap;

	public function new()
	{
		super();

		spriteMap = new Spritemap(Assets.getBitmapData("gfx/check.png"), 32, 32);
		spriteMap.add("unchecked", [0], 0, false);
		spriteMap.add("checked", [1], 0, false);
		this.graphic = spriteMap;

		this.layer = 0;
	}

	public function check()
	{
		spriteMap.play("checked");
	}

	public function uncheck()
	{
		spriteMap.play("unchecked");
	}

}