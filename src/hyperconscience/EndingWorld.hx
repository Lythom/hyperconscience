package hyperconscience ;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.MenuList;
import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import hyperconscience.GameWorld;
import openfl.Assets;
import openfl.events.KeyboardEvent;
import openfl.media.Sound;
import openfl.media.SoundChannel;
import openfl.text.TextFormatAlign;

/**
 * ...
 * @author Samuel Bouchet
 */

class EndingWorld extends Scene
{
	private var endText:Label;
	private var deathSound:Sound;
	private var deathSoundChannel:SoundChannel;
	private var deathCause:String;
	private var organCause:String;
	
	private var topText:Label;

	public static var instance:Scene ;

	public function new(death:Death)
	{
		super();

		switch( death) {
			case Asphyxia:
				deathSound = Assets.getSound("sfx/Hyperconscience_EndAttack.wav");
				deathCause = "d'asphyxie";
				organCause = "vos poumons";
			case Suffocate:
				deathSound = Assets.getSound("sfx/Hyperconscience_EndAttack.wav");
				deathCause = "de suffocation";
				organCause = "votre pharynx";
			case Attack:
				deathSound = Assets.getSound("sfx/Hyperconscience_EndAttack.wav");
				deathCause = "d'une crise cardiaque";
				organCause = "votre coeur";
			case Starving:
				deathSound = Assets.getSound("sfx/Hyperconscience_EndAttack.wav");
				deathCause = "de faim";
				organCause = "vos intestins";
			case StomachOverload:
				deathSound = Assets.getSound("sfx/Hyperconscience_EndAttack.wav");
				deathCause = "d'une indigestion";
				organCause = "votre estomac";
			case Breathlessness:
				deathSound = Assets.getSound("sfx/Hyperconscience_EndAttack.wav");
				deathCause = "d'essouflement";
				organCause = "vos poumons";
			default:
				deathSound = Assets.getSound("sfx/Hyperconscience_EndAttack.wav");
				deathCause = "d'une mort certaine";
				organCause = "";
		}

		// Texte d'introduction
		endText = new Label("Malheureusement, vous êtes mort " + deathCause + ".\nFaites attention à "+organCause+"\n\nAppuyez sur Espace pour redémarrer.");
		endText.font = Assets.getFont("font/vijaya.ttf").fontName;
		endText.color = 0x000000;
		endText.size = 36;
		endText.localY = 700;
		endText.localX = 768 / 2 - endText.width / 2;
		endText.layer = 0;
		
		topText = new Label("Trop dur ? Demandez de l'aide à un ami !");
		topText.font = Assets.getFont("font/vijaya.ttf").fontName;
		topText.color = 0x000000;
		topText.size = 36;
		topText.localY = 50;
		topText.localX = 768 / 2 - topText.width / 2;
		topText.layer = 0;

		instance = this;
		deathSoundChannel = deathSound.play(0, 1);
	}

	override public function update()
	{
		super.update();
		if (Input.pressed(Key.SPACE)) {
			HXP.scene = WelcomeWorld.instance;
		}
	}

	override public function begin()
	{
		super.begin();
		addGraphic(new Stamp(Assets.getBitmapData("gfx/deathscreen.jpg")));
		add(endText);
		add(topText);
	}

	override public function end()
	{
		removeAll();
		super.end();
	}

}