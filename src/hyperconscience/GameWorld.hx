package hyperconscience;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Panel;
import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import hyperconscience.core.Aliment;
import hyperconscience.core.Organe;
import hyperconscience.core.organs.Heart;
import hyperconscience.core.organs.Intestin;
import hyperconscience.core.organs.Lung;
import hyperconscience.core.organs.Pharynx;
import hyperconscience.core.organs.Stomach;
import openfl.Assets;
import openfl.geom.Point;
import openfl.media.Sound;
import openfl.media.SoundChannel;

/**
 * ...
 * @author Samuel Bouchet
 */

enum Death {
	Asphyxia;			// Poumons � 0
	Attack;				// Rythme cardiaque +/- : Coeur � 0 ou 200
	Breathlessness;		// Essoufflement : Poumons � 200
	Starving;			// Famine : Intestin � 0
	StomachOverload;	// Estomac explos� : Estomac � saturation (> NB_MAX_ALIMENTS)
	Suffocate;			// pharynx � 0
}

class GameWorld extends Scene
{
	private inline static var ALIMENTS_PER_TICK:Float = 0.01;

	var heart:Heart;
	var lung:Lung;
	var stomach:Stomach;
	var pharynx:Pharynx;
	var intestin:Intestin;

	var heartPosition:Point;
	var lungPosition:Point;
	var stomachPosition:Point;
	var pharynxPosition:Point;
	var intestinPosition:Point;
	var fond:Entity;
	var heartLabelPosition:Point;
	var lungLabelPosition:Point;
	var stomachLabelPosition:Point;
	var pharynxLabelPosition:Point;
	var intestinLabelPosition:Point;
	var CDAliment:Int = 0;

	public static var instance:Scene ;

	public function new()
	{
		super();

		instance = this;

		heartPosition = new Point(346 + 51, 335 + 65);
		lungPosition = new Point(212 + 158, 239 + 143);
		stomachPosition = new Point(356, 476 + 90);
		pharynxPosition = new Point(365, 165);
		intestinPosition = new Point(255 + 115, 616 + 118);

		heartLabelPosition = new Point(370, 305);
		lungLabelPosition = new Point(280, 215);
		stomachLabelPosition = new Point(416, 460);
		pharynxLabelPosition = new Point(340, 80);
		intestinLabelPosition = new Point(386, 555);

		heart = new Heart();
		heart.auto = true;
		heart.layer = 2;
		heart.touche.text = "D";
		lung = new Lung();
		lung.layer = 3;
		lung.touche.text = "S";
		pharynx = new Pharynx();
		pharynx.layer = 2;
		pharynx.touche.text = "U";
		stomach = new Stomach();
		stomach.layer = 4;
		stomach.touche.text = "J";
		intestin = new Intestin();
		intestin.layer = 2;
		intestin.touche.text = "N";

		var fondG:Spritemap = new Spritemap(Assets.getBitmapData("gfx/animfd.jpg"), 770, 1026);
		fondG.add("animate", [for (i in 0...45) i].concat([for (i in 0...45) 44-i]), 12);
		fondG.play("animate", true, false);
		fond = new Entity(0,0,fondG);
		fond.layer = 1000;

	}

	override public function update()
	{
		super.update();

		if (Input.pressed("heart")) {
			heart.contract();
			if (heart.auto) {
				heart.auto = false;
			}
		}
		if (Input.released("heart")) {
			heart.release();
		}
		if (Input.pressed("lung")) {
			lung.contract();
			if (lung.auto) {
				lung.auto = false;
			}
		}
		if (Input.released("lung")) {
			lung.release();
		}
		if (Input.pressed("pharynx")) {
			// d�glutit
			var alimentsToSendToStomach:Array<Aliment> = pharynx.eatableAliments;
			for (a in alimentsToSendToStomach) {
				//add(a);
				a.destination = stomachPosition;
				a.arrivedCallback = stomach.addAliment;
				a.y += 50;
			}
			pharynx.contract();

		}

		if (Input.pressed("stomach")) {
			// dig�re
			var alimentsToSendToIntestin:Array<Aliment> = stomach.aliments;
			for (a in alimentsToSendToIntestin) {
				//add(a);
				a.destination = intestin.intestinStartPosition;
				a.arrivedCallback = intestin.addAndConvertAliment;
			}
			stomach.contract();
		}

		if (Input.pressed("intestin")) {
			//tente d'assimiler un nutriment
			intestin.assimile();
		}

		if (Input.pressed(Key.ESCAPE)) {
			HXP.scene = WelcomeWorld.instance;
		}

		if (CDAliment > 0) {
			CDAliment --;
		}
		if (Math.random() < 0.005 && CDAliment == 0 && pharynx.scene != null) {
			var aliment:Aliment = new Aliment();
			aliment.x = pharynxPosition.x - 10;
			aliment.destination = new Point(pharynxPosition.x - 10, pharynxPosition.y);
			add(aliment);
			pharynx.addAliment(aliment);
			CDAliment = 60;
		}


		// V�rification des conditions de fin
		if (lung.isDead()) {
			if (lung.currentState <= 0) {
				gameOver(Asphyxia);
			}
			else {
				gameOver(Breathlessness);
			}
		}
		if (pharynx.isDead()) {
			gameOver(Suffocate);
		}
		if (heart.isDead()) {
			gameOver(Attack);
		}
		if (intestin.isDead()) {
			gameOver(Starving);
		}
		if (stomach.isDead()) {
			gameOver(StomachOverload);
		}

		if (heart.isValidated() && lung.scene != this) {
			add(lung);
		}
		if (lung.isValidated() && pharynx.scene != this) {
			add(pharynx);
		}
		if (pharynx.isValidated() && stomach.scene != this) {
			add(stomach);
		}
		if (stomach.isValidated() && intestin.scene != this) {
			add(intestin);
		}
	}

	private function sendToStomach(a:Aliment) {
		a.destination = stomachPosition;
	}

	override public function begin()
	{

		add(fond);

		heart.x = heartPosition.x;
		heart.y = heartPosition.y;
		add(heart);
		heart.touche.x = heartLabelPosition.x;
		heart.touche.y = heartLabelPosition.y;
		heart.instruction.x = heartLabelPosition.x;
		heart.instruction.y = heartLabelPosition.y - 10;

		lung.x = lungPosition.x;
		lung.y = lungPosition.y;
		//add(lung);
		lung.touche.x = lungLabelPosition.x;
		lung.touche.y = lungLabelPosition.y - 5;

		pharynx.x = pharynxPosition.x;
		pharynx.y = pharynxPosition.y;
		//add(pharynx);
		pharynx.touche.x = pharynxLabelPosition.x;
		pharynx.touche.y = pharynxLabelPosition.y;

		stomach.x = stomachPosition.x;
		stomach.y = stomachPosition.y;
		//add(stomach);
		stomach.touche.x = stomachLabelPosition.x;
		stomach.touche.y = stomachLabelPosition.y;

		intestin.x = intestinPosition.x;
		intestin.y = intestinPosition.y;
		//add(intestin);
		intestin.touche.x = intestinLabelPosition.x;
		intestin.touche.y = intestinLabelPosition.y;

		super.begin();
	}

	override public function end()
	{
		removeAll();
		super.end();
	}

	private function gameOver(deathCode:Death) {
		HXP.scene = new EndingWorld(deathCode);
	}

}