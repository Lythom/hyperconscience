package hyperconscience;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Stamp;
import openfl.Assets;

/**
 * ...
 * @author Lythom
 */
class LoadingScreen extends Entity
{
	public var addedToStage:Bool;
	public var startCD:Int = 10;

	public function new()
	{
		super(0, 0, new Stamp(Assets.getBitmapData("gfx/loadingscreen.jpg")));
	}

	override public function added():Void
	{
		super.added();
	}


	override public function update():Void
	{
		super.update();
		if (this.scene != null) {
			startCD --;
			if (startCD <= 0) {
				startCD = 0;
				this.addedToStage = true;
			}
		}
	}
}