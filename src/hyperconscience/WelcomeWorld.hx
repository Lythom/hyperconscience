package hyperconscience;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.MenuList;
import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import hyperconscience.GameWorld;
import openfl.Assets;
import openfl.events.KeyboardEvent;
import openfl.media.Sound;
import openfl.text.TextFormatAlign;

/**
 * ...
 * @author Samuel Bouchet
 */

class WelcomeWorld extends Scene
{
	private var bJouer:Button;
	private var title:Label;
	private var welcome:Label;
	private var music:Sound;
	var sound1:Sound;
	var loadingScreen:LoadingScreen;

	public static var instance:Scene ;

	public function new()
	{
		super();

		// Titre
		title = new Label("Hyper Conscience");
		title.font = Assets.getFont("font/lythgamescript.ttf").fontName;
		title.color = 0xFFFFFF;
		title.size = 96;

		// Texte d'introduction
		welcome = new Label("Avez-vous déjà réalisé tout ce que votre cerveau fait inconsciemment ?\nVous êtes à un doigt de le savoir.\n\n          Posez le sur Espace, et plongez dans l'inconscience !");
		welcome.font = Assets.getFont("font/pf_ronda_seven.ttf").fontName;
		welcome.color = 0xFFFFFF;
		welcome.size = 20;
		welcome.localY = 300;
		welcome.localX = 768 / 2 - welcome.width / 2;

		loadingScreen = new LoadingScreen();
		loadingScreen.layer = 0;

		music = Assets.getMusic("music/Hyperconscience_BackSkin.mp3");
		music.play(0,999);
		instance = this;

		sound1 = Assets.getSound("sfx/Hyperconscience_Inspiration01Norm.wav");

	}

	override public function update()
	{
		super.update();
		if (loadingScreen.addedToStage) {
			HXP.scene = new GameWorld();
		}
		if (Input.pressed(Key.SPACE) || Input.pressed(Key.ENTER)) {
			sound1.play();
			add(loadingScreen);
		}

	}

	override public function begin()
	{
		super.begin();
		addGraphic(new Stamp(Assets.getBitmapData("gfx/titlescreen.jpg")));
		//add(title);
		//add(welcome);
	}

	override public function end()
	{
		loadingScreen.addedToStage = false;
		removeAll();
		super.end();
	}

}