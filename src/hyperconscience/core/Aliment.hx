package hyperconscience.core;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.HXP;
import openfl.Assets;
import openfl.display.BitmapData;
import openfl.display.Graphics;
import openfl.display.Shape;
import openfl.geom.Point;
import tools.FastMath;

/**
 * ...
 * @author Lythom
 */
class Aliment extends Entity
{

	public var destination(get, set):Point;
	private var healthyness(get, null):Bool;

	private var nutrimentForm:Bool;
	var speed:Float = 1; // px / tick
	var angle:Float = 0;
	public var arrived:Bool = false;
	public var arrivedCallback:Aliment->Void = null;

	private inline static var ALIMENT_COLOR:UInt = 0xFF8040;
	private inline static var HEALTHY_COLOR:UInt = 0x008000;
	private inline static var UNHEALTHY_COLOR:UInt = 0x9D0000;

	public function new(healthyness:Bool = true, nutrimentForm:Bool = false)
	{
		super();
		this.healthyness = healthyness;
		this.nutrimentForm = nutrimentForm;

		this.width = 50;
		this.graphic = new Stamp(Assets.getBitmapData("gfx/nourriture.png"));

		destination = new Point();

	}

	public function convert() {

		if (!nutrimentForm) {
			nutrimentForm = true;
			if (Math.random() < 0.3) {
				healthyness = false;
			}
			if (healthyness) {
				graphic = new Stamp(Assets.getBitmapData("gfx/nutriment_bon.png"));
			}
			else{
				graphic = new Stamp(Assets.getBitmapData("gfx/nutriment_mauvais.png"));
			}
		}
		layer = 0;
	}

	override public function update():Void
	{
		super.update();

		if (Math.abs(this.x - destination.x) > speed) {
			this.x += speed * HXP.sign(destination.x - x);
		} else {
			this.x = destination.x;
		}
		if (Math.abs(this.y - destination.y) > speed) {
			this.y += speed * HXP.sign(destination.y - y);
		} else {
			this.y = destination.y;
		}

		if (this.x == destination.x && this.y == destination.y && !arrived) {
			arrived = true;
			if (arrivedCallback != null) {
     			arrivedCallback(this);
			}
		}
	}

	private var _destination:Point;
	function get_destination():Point
	{
		return _destination;
	}

	function set_destination(value:Point):Point
	{
		arrived = false;
		return _destination = value;
	}

	public function get_healthyness():Bool
	{
		return healthyness;
	}

}