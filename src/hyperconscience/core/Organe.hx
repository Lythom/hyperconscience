package hyperconscience.core;
import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.gui.Label;
import com.haxepunk.Mask;
import hyperconscience.core.Phase;
import openfl.Assets;
import openfl.display.BitmapData;
import openfl.display.Shape;
import openfl.filters.DropShadowFilter;

/**
 * ...
 * @author Lythom
 */
class Organe extends Entity
{
	private inline static var DEADLY_MAX_ERROR_TIME:Int = 3000;
	private inline static var STATE_MAX_AMPLITUDE:Int = 100;
	public inline static var TIME_TO_STATE_RATIO:Float = STATE_MAX_AMPLITUDE / DEADLY_MAX_ERROR_TIME;
	public inline static var STATE_TO_TIME_RATIO:Float = DEADLY_MAX_ERROR_TIME / STATE_MAX_AMPLITUDE;

	var animatedSprite:Spritemap;
	var animatedShape:Spritemap;

	public var phaseContract:Phase;
	public var phaseRelease:Phase;

	public var currentPhase:Phase;

	public var currentState:Float;

	public var touche:Label;

	public function new(contractPhaseTimePerfect:Int = 0, releasePhaseTimePerfect:Int = 0) {
		super();

		phaseContract = new Phase(contractPhaseTimePerfect);
		phaseRelease = new Phase(releasePhaseTimePerfect);

		currentPhase = phaseRelease;
		currentState = Organe.STATE_MAX_AMPLITUDE;

		touche = new Label();
		touche.bitmapBufferMargin = 30;
		touche.size = 24;
		touche.color = 0xFFFFFF;
		touche.textField.filters = [new DropShadowFilter(1, 135, 0x000000, 1, 6, 6, 2, 1, false)];
		touche.layer = 0;
		touche.font = Assets.getFont("font/vijaya.ttf").fontName;
	}

	/**
	 * Déclenché par l'utilisateur. Le muscle de l'organe se contracte.
	 */
	public function contract():Void {

	}

	/*
	 * Déclenché par l'utilisateur. Le muscle de l'organe se relache.
	 */
	public function release():Void {

	}

	public function gotoNextPhase():Void {
		if (currentPhase == phaseContract) {
			switchToPhase(phaseRelease);
		} else {
			switchToPhase(phaseContract);
		}
	}

	public function switchToPhase(phase:Phase):Void	{
		currentPhase = phase;
		currentPhase.reset();
	}

	override public function update():Void
	{
		super.update();

		if (this.currentPhase != null) {
			this.currentPhase.update();
		}
	}


	/**
	 * Mise à jour de l'état.
	 * deltaValue : delta à appliquer, positif ou négatif
	 */
	public function addToCurrentState(deltaValue:Float):Void {
		this.currentState += deltaValue * TIME_TO_STATE_RATIO;
	}


	public function isDead():Bool {
		return currentState <= 0 || currentState >= 200;
	}

	public function isValidated():Bool {
		return false;
	}

	override public function added():Void
	{
		super.added();
		if (touche.scene == null) {
			this.scene.add(touche);
		}
	}


}