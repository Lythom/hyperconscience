package hyperconscience.core;
import com.haxepunk.HXP;

/**
 * ...
 * @author Lythom
 */
class Phase
{

	// In milliseconds
	public var timePerfect:Int;

	// In milliseconds
	public var timeActual:Int;

	/**
	 * In milliseconds
	 */
	public var timeDelta(get, null):Float;
	function get_timeDelta():Float
	{
		return (timePerfect - timeActual);
	}

	/**
	 *
	 * @param	timePerfect		temps idéal de durée de la phase
	 */
	public function new(timePerfect:Int)
	{
		this.timeActual = 0;
		this.timePerfect = timePerfect;
	}

	/**
	 * Incrémente le temps réel.
	 */
	public function update() {
		timeActual += Math.round(HXP.elapsed * 1000);
	}

	/**
	 * Revient à 0 en attente du nouveau début de phase.
	 */
	public function reset() {
		timeActual = 0;
	}



}