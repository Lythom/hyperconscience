package hyperconscience.core;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.HXP;
import com.haxepunk.Mask;
import openfl.display.BitmapData;
import openfl.display.Graphics;
import openfl.display.Shape;
import openfl.filters.GlowFilter;
import openfl.filters.BitmapFilter;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Vincent Boulanger
 */
class TimerCircle extends Entity
{
	private var renderRect:Rectangle;
	private var circleBuffer:BitmapData;
	private var shape:Shape;
	private var state:Float;
	private var step:Int;
	private var radius:Int;

	private var lineWidth:Int;
	private var lineColor:UInt;
	private var bufferPadding:Int;
	private var bitmapFilter:GlowFilter;

	public function new(width:Int = 0, height:Int = 0)
	{
		super();
		this.height = height;
		this.width = width;
		this.step = 96;
		this.lineWidth = 5;
		this.lineColor = 0xffffff;
		this.bufferPadding = 20;

		if (width >= height) {
			radius = Math.round(width / 2);
		} else {
			radius = Math.round(height / 2);
		}
		circleBuffer = HXP.createBitmap(radius * 2 + bufferPadding, radius * 2 + bufferPadding, true);
		this.graphic = new Stamp(circleBuffer);
		this.graphic.x = Math.ceil(-circleBuffer.width/2);
		this.graphic.y = Math.ceil(-circleBuffer.height/2);

		state = 0;
		shape = new Shape();
		renderRect = new Rectangle(0, 0, radius * 2 + bufferPadding, radius * 2 + bufferPadding);
		layer = 0;

		bitmapFilter = getBitmapFilter();

		shape.filters = [bitmapFilter];
	}

	private function getBitmapFilter():GlowFilter {
		var color:UInt = 0xFFFFFF;
		var alpha:Float = 0.8;
		var blurX:Int = 5;
		var blurY:Int = 5;
		var strength:Int = 2;

		return new GlowFilter(color,
							  alpha,
							  blurX,
							  blurY,
							  strength);
	}

	override public function render():Void
	{
		super.render();
		circleBuffer.fillRect(renderRect, HXP.blackColor);
		circleBuffer.draw(shape);
	}

	public function updateState(newValue:Float) {
		state = newValue;
		if (state <= 100) {
			var shapeGraphics:Graphics = shape.graphics;
			shapeGraphics.clear();
			shapeGraphics.lineStyle(lineWidth, lineColor, 1);
			shapeGraphics.moveTo(bufferPadding/2 + 2, radius + bufferPadding/2 + 2);

			var progress:Int = Math.ceil(step * state / 100);
			for (i in 1...progress+1) {
				var angle = (Math.PI * 2 / step) * i + Math.PI;
				shapeGraphics.lineTo(Math.cos(angle) * radius + radius + bufferPadding/2 + 2, Math.sin(angle) * radius + radius + bufferPadding/2 + 2);
			}
		}
	}

	public function changeLineStyle(timerLineColor:UInt, timerLineWidth:Int)
	{
		this.lineColor = timerLineColor;
		this.lineWidth = timerLineWidth;
		this.bufferPadding = lineWidth * 2;
		bitmapFilter.inner = !bitmapFilter.inner;
	}
}