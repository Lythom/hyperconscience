package hyperconscience.core.organs;

import com.haxepunk.HXP;
import hyperconscience.core.Aliment;
import hyperconscience.core.Organe;

/**
 * ...
 * @author Lythom
 */
class AlimentaryOrgan extends Organe
{
	private inline static var DELTA_STATE_ON_USELESS_CONTRACTION = 10;
	
	private inline static var DEADLY_MAX_ERROR_TIME:Int = 5000;
	private inline static var STATE_MAX_AMPLITUDE:Int = 100;
	public inline static var TIME_TO_STATE_RATIO:Float = STATE_MAX_AMPLITUDE / DEADLY_MAX_ERROR_TIME;
	public inline static var STATE_TO_TIME_RATIO:Float = DEADLY_MAX_ERROR_TIME / STATE_MAX_AMPLITUDE;

	var succeededOnce:Bool = false;


	public var aliments:Array<Aliment>; // aliments en attente de déglutition


	public function new(contractPhaseTimePerfect:Int = 0, releasePhaseTimePerfect:Int = 0)
	{
		super(contractPhaseTimePerfect, releasePhaseTimePerfect);
		aliments = new Array<Aliment>();
	}

	override public function update():Void
	{
		super.update();
	}

	override public function isValidated():Bool
	{
		return succeededOnce;
	}

	public function addAliment(a:Aliment) {
		aliments.push(a);
	}

	override public function contract():Void
	{
		super.contract();

		// déglutit
		if (aliments.length > 0) {
			// repasse en attente
			if (currentState < Organe.STATE_MAX_AMPLITUDE) {
				currentState = Organe.STATE_MAX_AMPLITUDE;
			}
			currentPhase = null;
			aliments.splice(0, aliments.length);
			succeededOnce = true;

		} else {
			// c'était pas la peine !
			currentState += DELTA_STATE_ON_USELESS_CONTRACTION;
		}

	}

	override public function addToCurrentState(deltaValue:Float):Void {
		this.currentState += deltaValue * TIME_TO_STATE_RATIO;
	}

	override public function release():Void
	{
		super.release();
	}

}