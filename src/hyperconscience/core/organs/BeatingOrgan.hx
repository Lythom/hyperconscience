package hyperconscience.core.organs;
import com.haxepunk.animation.AnimatedSprite;
import com.haxepunk.animation.BitmapFrame;
import com.haxepunk.animation.Sequence;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import hyperconscience.Check;
import hyperconscience.core.TimerCircle;
import openfl.Assets;
import openfl.geom.Point;
import openfl.media.Sound;
import openfl.media.SoundChannel;

/**
 * ...
 * @author Lythom
 */
class BeatingOrgan extends Organe
{
	public inline static var CONTRACT_COLOR = 0xC76B6D;
	public inline static var RELEASE_COLOR = 0x5A65C0;
	
	private inline static var SUCCESS_IN_A_ROW_TO_GO_NEXT = 6;
	private inline static var SUCCESS_SENSIVITY_IN_MS = 250;
	private var successInARow:Int = 0;

	private inline static var RECOVER_TIME_RATIO = 0.10;
	private var circleTimer:TimerCircle;

	private var sound1:Sound;
	private var soundChannel1:SoundChannel;
	private var sound2:Sound;
	private var soundChannel2:SoundChannel;

	private var checks:Array<Check>;
	
	private var _auto:Bool = false;


	public function new(contractPhaseTimePerfect:Int = 0, releasePhaseTimePerfect:Int = 0)
	{
		super(contractPhaseTimePerfect, releasePhaseTimePerfect);

		checks = new Array<Check>();
		for (c in 0...SUCCESS_IN_A_ROW_TO_GO_NEXT) {
			checks.push(new Check());
		}
	}

	override public function contract():Void
	{
		super.contract();
		animatedSprite.play("contract");

		if (soundChannel2 != null) {
			soundChannel2.stop();
		}
		soundChannel1 = sound1.play(0, 0);

		// Récupération du différentiel entre le temps écoulé et le temps attendu
		var delta:Float = phaseRelease.timeDelta;
		checkSuccess(delta);
		// Cas de sur-régime, contraction démarrée trop tôt
		if (delta > 0) {
			addToCurrentState(delta);
		}

		switchToPhase(this.phaseContract);
		switchLineStyle();
	}

	override public function release():Void
	{
		super.release();
		animatedSprite.play("release");
		if (soundChannel1 != null) {
			soundChannel1.stop();
		}
		soundChannel2 = sound2.play(0, 0);

		// Récupération du différentiel entre le temps écoulé et le temps attendu
		var delta:Float = phaseContract.timeDelta;
		checkSuccess(delta);
		// Cas de sur-régime, contraction achevée trop tôt
		if (delta > 0) {
			addToCurrentState(delta);
		}
		switchToPhase(this.phaseRelease);
		switchLineStyle();
	}

	private function checkSuccess(delta:Float) {
		if(!isValidated() && !auto){
			if (Math.abs(delta) < SUCCESS_SENSIVITY_IN_MS) {
				successInARow += 1;
				for (c in 0...successInARow) {
					checks[c].check();
				}
				if (isValidated()) {
					for (c in 0...checks.length) {
						if (checks[c].scene != null) {
							checks[c].scene.remove(checks[c]);
						}
					}
				}
			} else {
				successInARow = 0;
				for (c in 0...checks.length) {
					checks[c].uncheck();
				}
			}
		}

	}

	override public function isValidated():Bool
	{
		return successInARow >= SUCCESS_IN_A_ROW_TO_GO_NEXT;
	}

	override public function update():Void
	{
		super.update();
		// Récupération du différentiel entre le temps écoulé et le temps attendu
		var delta:Float = currentPhase.timeDelta;
		if (delta >= 0) {
			if(currentState != Organe.STATE_MAX_AMPLITUDE){
				var tempState = HXP.elapsed * 1000 * RECOVER_TIME_RATIO * HXP.sign(Organe.STATE_MAX_AMPLITUDE - currentState);
				if (Math.abs(Organe.STATE_MAX_AMPLITUDE - currentState) <= tempState) {
					currentState = Organe.STATE_MAX_AMPLITUDE;
				}
				else {
					addToCurrentState(tempState);
				}
			}
		}
		else {
			if (auto) {
				if (this.currentPhase == this.phaseRelease) {
					contract();
				} else {
					release();
				}
			} else {
				addToCurrentState(-(HXP.elapsed * 1000));
			}
		}
		animatedSprite.scale = Math.max(Math.min(currentState / 100, 1), 0);

		// update du timer en cercle
		if(currentPhase != null && circleTimer != null){
			circleTimer.updateState((currentPhase.timeActual / currentPhase.timePerfect) * 100);
		}
	}

	override public function added():Void
	{
		super.added();
		if (circleTimer.scene != this.scene && circleTimer != null) {
			this.scene.add(circleTimer);
		}
		if (!auto) {
			placeChecks();
		}
	}

	function placeChecks()
	{
		if(animatedShape != null){
			var i:Int = 0;
			for (c in checks) {
				c.x = this.x + animatedShape.x + (animatedShape.width / 2 + 30) * Math.cos((Math.PI / 10) * i);
				c.y = this.y + animatedShape.y + (animatedShape.height / 2 + 30) * Math.sin((Math.PI / 10) * i);
				this.scene.add(c);
				i++;
			}
		}
	}

	override public function removed():Void
	{
		super.removed();
		if (circleTimer.scene == this.scene) {
			this.scene.remove(circleTimer);
		}
	}

	public function switchLineStyle() {
		var timerLineColor:UInt;
		var timerLineWidth:Int;
		if (currentPhase == phaseContract) {
			timerLineColor = BeatingOrgan.CONTRACT_COLOR;
			timerLineWidth = 5;
		}
		else {
			timerLineColor = BeatingOrgan.RELEASE_COLOR;
			timerLineWidth = 2;
		}
		circleTimer.changeLineStyle(timerLineColor, timerLineWidth);
	}
	
	function get_auto():Bool 
	{
		return _auto;
	}
	
	function set_auto(value:Bool):Bool 
	{
		if (!value) {
			placeChecks();
		}
		return _auto = value;
	}
	
	public var auto(get_auto, set_auto):Bool;
}