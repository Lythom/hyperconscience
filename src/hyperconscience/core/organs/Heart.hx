package hyperconscience.core.organs;
import com.haxepunk.animation.AnimatedSprite;
import com.haxepunk.animation.BitmapFrame;
import com.haxepunk.animation.Sequence;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.gui.Label;
import com.haxepunk.HXP;
import openfl.Assets;
import openfl.filters.DropShadowFilter;
import openfl.filters.GlowFilter;
import openfl.geom.Point;
import openfl.media.Sound;

/**
 * ...
 * @author Lythom
 */
class Heart extends BeatingOrgan
{
	private inline static var CONTRACT_TIME = 520;
	private inline static var RELEASE_TIME = 960;
	var filter:GlowFilter;
	public var instruction:Label;

	public function new()
	{
		super(CONTRACT_TIME, RELEASE_TIME);
		animatedSprite = new Spritemap(Assets.getBitmapData("gfx/heartAnim.png"), 93, 128);
		animatedSprite.add("contract", [for (i in 0...12) i], 24, false);
		animatedSprite.add("release", [for (i in 0...12) 12 - i], 24, false);
		animatedSprite.centerOrigin();

		// 512x1822 => 5x14 => 70
		animatedShape = new Spritemap(Assets.getBitmapData("gfx/trem_coeur.png"), 102, 130);
		animatedShape.add("still", [0], 0, false);
		animatedShape.add("attention", [for (i in 0...22) i], 12, true);
		animatedShape.add("warn", [for (i in 22...44) i], 24, true);
		animatedShape.add("danger", [for (i in 44...70) i], 48, true);
		animatedShape.centerOrigin();

		this.graphic = new Graphiclist([animatedSprite, animatedShape]);

		circleTimer = new TimerCircle(102, 130);
		switchLineStyle();

		sound1 = Assets.getSound("sfx/Hyperconscience_CoeurNorm01.wav");
		sound2 = Assets.getSound("sfx/Hyperconscience_CoeurNorm02.wav");
		
		instruction = new Label();
		instruction.font = Assets.getFont("font/vijaya.ttf").fontName;
		instruction.color = BeatingOrgan.RELEASE_COLOR;
		instruction.size = 32;
		instruction.layer = 0;
		instruction.textField.filters = [new DropShadowFilter(1, 135, 0x000000, 1, 6, 6, 2, 1, false)];
	}
	
	override public function contract():Void 
	{
		if (instruction.scene != null) {
			instruction.color = BeatingOrgan.CONTRACT_COLOR;
			instruction.text = "Maintenir";
		}
		super.contract();
	}
	
	override public function release():Void 
	{
		if (instruction.scene != null) {
			instruction.color = BeatingOrgan.RELEASE_COLOR;
			instruction.text = "Relâcher";
		}
		super.release();
	}
	
	override public function added():Void 
	{
		super.added();
		this.scene.add(instruction);
	}
	
	override private function set_auto(value:Bool):Bool 
	{
		if (!value && instruction.scene != null) {
			instruction.scene.remove(instruction);
		}
		return super.set_auto(value);
	}


	override public function update():Void
	{
		super.update();
		
		if (animatedShape != null) {
			circleTimer.x = this.x + 5;
			circleTimer.y = this.y + 5;
		}

		if (currentState >= 170) {
			animatedShape.play("danger");
		} else if (currentState >= 140) {
			animatedShape.play("warn");
		} else if (currentState >= 110) {
			animatedShape.play("attention");
		} else {
			animatedShape.play("still");
		}

		if (currentState <= 50) {
			sound1 = Assets.getSound("sfx/Hyperconscience_CoeurInf01.wav");
			sound2 = Assets.getSound("sfx/Hyperconscience_CoeurInf02.wav");

		} else if (currentState >= 150) {
			sound1 = Assets.getSound("sfx/Hyperconscience_CoeurSup01.wav");
			sound2 = Assets.getSound("sfx/Hyperconscience_CoeurSup02.wav");

		}else {
			sound1 = Assets.getSound("sfx/Hyperconscience_CoeurNorm01.wav");
			sound2 = Assets.getSound("sfx/Hyperconscience_CoeurNorm02.wav");
		}
	}
}