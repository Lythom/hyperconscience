package hyperconscience.core.organs;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.HXP;
import openfl.Assets;
import openfl.display.BitmapData;
import hyperconscience.core.Aliment;
import openfl.display.Shape;
import openfl.filters.BitmapFilter;
import openfl.filters.GlowFilter;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.media.Sound;
import openfl.media.SoundChannel;

/**
 * ...
 * @author Lythom
 */
class Intestin extends AlimentaryOrgan
{

	private inline static var CONTRACT_TIME = -1;
	private inline static var RELEASE_TIME = 5000;
	private inline static var HUNGER_INCREMENT_RATIO = 0.07;
	private inline static var SUCCESS_BONUS_VALUE = 10;
	private inline static var FAIL_MALUS_VALUE = 2;
	public var intestinStartPosition:Point;
	private var intestinEndPosition:Point;
	private var deltaDeparture:Float;
	private var activeArea:Entity;
	private var nbAlimentOnScene:Int;


	private var sound1:Sound;
	private var soundChannel1:SoundChannel;

	private var sound2:Sound;
	private var soundChannel2:SoundChannel;

	public var alimentsToProcess:Array<Aliment>; // aliments en attente de traitement (assmiliation ou drop)

	public function new()
	{
		super(CONTRACT_TIME, RELEASE_TIME);

		animatedSprite = new Spritemap(Assets.getBitmapData("gfx/org_tubedigestif.png"), 231, 236);
		animatedSprite.add("still", [0], 0, false);
		animatedSprite.play("still");
		animatedSprite.centerOrigin();
		animatedSprite.x -= 5;

		animatedShape = new Spritemap(Assets.getBitmapData("gfx/membrane_intestins.png"), 231, 236);
		animatedShape.add("still", [0], 0, false);
		animatedShape.play("still");
		animatedShape.centerOrigin();

		this.graphic = new Graphiclist([animatedSprite, animatedShape]);

		alimentsToProcess = new Array<Aliment>();
		intestinStartPosition = new Point(275, 670);
		intestinEndPosition = new Point(470, 670);
		deltaDeparture = 0;
		currentState = 100;
		nbAlimentOnScene = 0;

		activeArea = new Entity(360, 654, new Stamp(Assets.getBitmapData("gfx/circle.png")));
		activeArea.width = 100;
		activeArea.layer = 1;

		sound1 = Assets.getSound("sfx/Hyperconscience_NutrimentsAbsorbe.wav");
		sound2 = Assets.getSound("sfx/Hyperconscience_NutrimentsFail.wav");
	}

	public function addAndConvertAliment(a:Aliment) {
		addAliment(a);
		a.convert();
	}

	override public function update():Void
	{
		super.update();
		if (this.aliments.length > 0) {
			if (deltaDeparture < 2000) {
				deltaDeparture += HXP.elapsed * 1000;
				//trace(deltaDeparture);
			}
			else {
				var a:Aliment = this.aliments.shift();
				alimentsToProcess.push(a);
				nbAlimentOnScene++;
				if (alimentsToProcess.length == 1) {
					this.scene.add(activeArea);
				}
				a.destination = intestinEndPosition;
				a.arrivedCallback = this.dropAliment;
				deltaDeparture = 0;
			}
		}
		var tempState = HXP.elapsed * 1000 * HUNGER_INCREMENT_RATIO;
		addToCurrentState( -tempState);
		this.animatedSprite.scale = Math.min(1, currentState / 100);
		if (currentState > 150) {
			currentState = 150;
		}
	}

	public function dropAliment(a:Aliment) {
		a.scene.remove(a);
		nbAlimentOnScene--;
		alimentsToProcess.remove(a);
		if (alimentsToProcess.length == 0) {
			this.scene.remove(activeArea);
		}
	}

	public function assimile()
	{
		var success:Bool = false;
		var delta:Float = 0;
		var it:Iterator<Aliment> = this.alimentsToProcess.iterator();
		while (it.hasNext()) {
			var a:Aliment = it.next();
			if (a.x >= activeArea.x && a.x + a.width <= activeArea.x + activeArea.width) {
				delta = 1-(Math.abs(a.centerX - activeArea.centerX) / (activeArea.width/2 + a.width/2));
				dropAliment(a);
				if (a.get_healthyness()) {
					success = true;
				}
			}
		}
		if (success) {
			soundChannel1 = sound1.play(0, 1);
			currentState += delta * SUCCESS_BONUS_VALUE;
		}
		else {
			soundChannel2 = sound2.play(0, 1);
			currentState -= FAIL_MALUS_VALUE;
		}
	}

}