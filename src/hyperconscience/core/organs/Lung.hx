package hyperconscience.core.organs;
import com.haxepunk.animation.AnimatedSprite;
import com.haxepunk.animation.BitmapFrame;
import com.haxepunk.animation.Sequence;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import hyperconscience.core.TimerCircle;
import openfl.Assets;
import openfl.geom.Point;

/**
 * ...
 * @author Lythom
 */
class Lung extends BeatingOrgan
{
	private inline static var CONTRACT_TIME = 1200;
	private inline static var RELEASE_TIME = 1200;

	public function new()
	{
		super(CONTRACT_TIME, RELEASE_TIME);
		// 1575x1128 => 5x4
		animatedSprite = new Spritemap(Assets.getBitmapData("gfx/poumonAnim.png"), 317, 285);
		animatedSprite.add("contract", [for (i in 0...18) i], 24, false);
		animatedSprite.add("release", [for (i in 0...18) 18 - i], 24, false);
		animatedSprite.centerOrigin();
		animatedSprite.x += 7;
		animatedSprite.y += 5;

		// 2009x3170 => 10x7 => 70
		animatedShape = new Spritemap(Assets.getBitmapData("gfx/tremb_poumon.png"), 317, 287);
		animatedShape.add("still", [0], 0, false);
		animatedShape.add("attention", [for (i in 0...11) i], 12, true);
		animatedShape.add("warn", [for (i in 11...22) i], 24, true);
		animatedShape.add("danger", [for (i in 22...35) i], 48, true);
		animatedShape.centerOrigin();

		this.graphic = new Graphiclist([animatedSprite, animatedShape]);

		circleTimer = new TimerCircle(373, 346);
		circleTimer.x = 212;
		circleTimer.y = 190;
		switchLineStyle();

		sound1 = Assets.getSound("sfx/Hyperconscience_ExpirationNorm.wav");
		if (Math.random() < 0.5) {
			sound2 = Assets.getSound("sfx/Hyperconscience_Inspiration01Norm.wav");
		}
		else {
			sound2 = Assets.getSound("sfx/Hyperconscience_Inspiration02Norm.wav");
		}
	}

	override public function update():Void
	{
		super.update();
		
		if (animatedShape != null) {
			circleTimer.x = this.x + 10;
			circleTimer.y = this.y + 25;
		}
		
		if (currentState >= 170) {
			animatedShape.play("danger");
		} else if (currentState >= 140) {
			animatedShape.play("warn");
		} else if (currentState >= 110) {
			animatedShape.play("attention");
		} else {
			animatedShape.play("still");
		}
		
		if (currentState <= 50) {
			sound1 = Assets.getSound("sfx/Hyperconscience_ExpirationInf.wav");
			if (Math.random() < 0.5) {
				sound2 = Assets.getSound("sfx/Hyperconscience_Inspiration01Inf.wav");
}
			else {
				sound2 = Assets.getSound("sfx/Hyperconscience_Inspiration02Inf.wav");
			}

		}else if (currentState >= 150) {
			sound1 = Assets.getSound("sfx/Hyperconscience_ExpirationSup.wav");
			if (Math.random() < 0.5) {
				sound2 = Assets.getSound("sfx/Hyperconscience_Inspiration01Sup.wav");
}
			else {
				sound2 = Assets.getSound("sfx/Hyperconscience_Inspiration02Sup.wav");
			}

		}else {
			sound1 = Assets.getSound("sfx/Hyperconscience_ExpirationNorm.wav");
			if (Math.random() < 0.5) {
				sound2 = Assets.getSound("sfx/Hyperconscience_Inspiration01Norm.wav");
			}
			else {
				sound2 = Assets.getSound("sfx/Hyperconscience_Inspiration02Norm.wav");
			}
		}
	}

}