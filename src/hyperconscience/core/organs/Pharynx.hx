package hyperconscience.core.organs;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.HXP;
import hyperconscience.core.Aliment;
import hyperconscience.core.Organe;
import openfl.Assets;
import openfl.media.Sound;
import openfl.media.SoundChannel;

/**
 * ...
 * @author Lythom
 */
class Pharynx extends AlimentaryOrgan
{

	private inline static var CONTRACT_TIME = -1;
	private inline static var RELEASE_TIME = 5000;

	private inline static var RECOVER_TIME_RATIO = 0.5;

	private var sound1:Sound;
	private var soundChannel1:SoundChannel;
	public var arrivedAliments:Array<Aliment>;
	public var eatableAliments:Array<Aliment>;
	public var circle:Entity;

	public function new()
	{
		super(CONTRACT_TIME, RELEASE_TIME);
		sound1 = Assets.getSound("sfx/Hyperconscience_DeglutitionNorm.wav");

		animatedShape = new Spritemap(Assets.getBitmapData("gfx/tremb_pharynx.png"), 163, 163);
		animatedShape.add("still", [0], 0, false);
		animatedShape.add("attention", [for (i in 0...22) i], 12, true);
		animatedShape.add("warn", [for (i in 22...44) i], 24, true);
		animatedShape.add("danger", [for (i in 44...72) i], 48, true);
		animatedShape.scale = 0.7;
		animatedShape.centerOrigin();
		animatedShape.x += 10;
		animatedShape.y -= 15;
		
		var circleGraphic:Image = new Image(Assets.getBitmapData("gfx/circle_white.png"));
		circleGraphic.centerOrigin();
		circle = new Entity(0, 0, circleGraphic);
		
		circle.x = 376;
		circle.y = 149;

		graphic = animatedShape;
	}
	
	override public function added():Void 
	{
		super.added();
		this.scene.add(circle);
	}

	override public function addAliment(a:Aliment)
	{
		super.addAliment(a);
	}

	override public function update():Void
	{
		super.update();

		if (currentState >= 170) {
			animatedShape.play("danger");
		} else if (currentState >= 140) {
			animatedShape.play("warn");
		} else if (currentState >= 110) {
			animatedShape.play("attention");
		} else {
			animatedShape.play("still");
		}

		// attends un aliment
		arrivedAliments = aliments.filter(isArrived);
		if (arrivedAliments.length > 0) {

			addToCurrentState(HXP.elapsed * 1000 * arrivedAliments.length *0.90);

		} else {
			// retour vers une situation d'équilibre si l'organe est vide
			if (currentState != Organe.STATE_MAX_AMPLITUDE) {
				var recoverDelta = HXP.elapsed * 1000 * RECOVER_TIME_RATIO * HXP.sign(Organe.STATE_MAX_AMPLITUDE - currentState);
				if (Math.abs(Organe.STATE_MAX_AMPLITUDE - currentState) <= recoverDelta) {
					currentState = Organe.STATE_MAX_AMPLITUDE;
				}
				else {
					addToCurrentState(recoverDelta);
				}
			}
		}
		
		// gère le cercle
		eatableAliments = aliments.filter(eatable);
		if (eatableAliments.length > 0 && circle.scene == null) {
			this.scene.add(circle);
			if (Std.is(this.touche.graphic, Image)) {
				cast(this.touche.graphic, Image).scale = 1.3;
			}
		}
		if (eatableAliments.length == 0 && circle.scene != null) {
			circle.scene.remove(circle);
			if (Std.is(this.touche.graphic, Image)) {
				cast(this.touche.graphic, Image).scale = 1;
			}
		}

		if (currentState <= 50) {
			sound1 = Assets.getSound("sfx/Hyperconscience_DeglutitionInf.wav");

		}else if (currentState >= 150) {
			sound1 = Assets.getSound("sfx/Hyperconscience_DeglutitionSup.wav");

		}else {
			sound1 = Assets.getSound("sfx/Hyperconscience_DeglutitionNorm.wav");
		}
		
		circle.x = this.x + 9;
		circle.y = this.y - 13;
	}
	
	function eatable(a:Aliment):Bool 
	{
		return a.y > circle.y - 70;
	}
	
	function isArrived(a:Aliment):Bool 
	{
		return a.arrived == true;
	}

	override public function contract():Void
	{
		// déglutit
		if (eatableAliments.length > 0) {
			// repasse en attente
			if (currentState < Organe.STATE_MAX_AMPLITUDE) {
				currentState = Organe.STATE_MAX_AMPLITUDE;
			}
			currentPhase = null;
			for (a in eatableAliments) {
				aliments.remove(a);
			}
			succeededOnce = true;

		} else {
			// c'était pas la peine !
			currentState += AlimentaryOrgan.DELTA_STATE_ON_USELESS_CONTRACTION;
		}
		
		
		if (soundChannel1 != null) {
			soundChannel1.stop();
		}
		soundChannel1 = sound1.play(0, 0);
	}

	override public function release():Void
	{
		// unused
		super.release();
	}

}