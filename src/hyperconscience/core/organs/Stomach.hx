package hyperconscience.core.organs;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import hyperconscience.core.Aliment;
import hyperconscience.core.Organe;
import openfl.Assets;
import openfl.media.Sound;
import openfl.media.SoundChannel;

/**
 * ...
 * @author Lythom
 */
class Stomach extends AlimentaryOrgan
{

	private inline static var NB_MAX_ALIMENTS = 14;

	private var sound1:Sound;
	private var soundChannel1:SoundChannel;

	private var activityCDInMS:Int = 0;
	var activated:Bool = false;

	public function new()
	{
		super( -1, -1);

		animatedSprite = new Spritemap(Assets.getBitmapData("gfx/stomacAnim.png"), 157, 178);
		animatedSprite.add("active", [for (i in 0...36) i], 24, true);
		animatedSprite.add("still", [0], 24, false);
		animatedSprite.centerOrigin();
		animatedSprite.x -= 5;

		animatedShape = new Spritemap(Assets.getBitmapData("gfx/tremb_estomac.png"), 160, 181);
		animatedShape.add("still", [0], 0, false);
		animatedShape.add("attention", [for (i in 0...22) i], 12, true);
		animatedShape.add("warn", [for (i in 22...44) i], 24, true);
		animatedShape.add("danger", [for (i in 50...70) i], 48, true);
		animatedShape.centerOrigin();

		this.graphic = new Graphiclist([animatedSprite, animatedShape]);

		sound1 = Assets.getSound("sfx/Hyperconscience_Estomac90.wav");
		
		currentState = Organe.STATE_MAX_AMPLITUDE;
	}

	override public function update():Void
	{
		super.update();

		if (this.activated) {
			activityCDInMS -= Math.round(HXP.elapsed * 1000);
			if (activityCDInMS <= 0) {
				activityCDInMS = 0;
				desactivate();
			}
		}

		if (currentState >= 170) {
			animatedShape.play("danger");
		} else if (currentState >= 140) {
			animatedShape.play("warn");
		} else if (currentState >= 110) {
			animatedShape.play("attention");
		} else {
			animatedShape.play("still");
		}
	}

	public function activate() {
		this.activated = true;
		animatedSprite.play("active");
	}
	public function desactivate() {
		this.activated = false;
		animatedSprite.play("still");
	}

	override public function contract():Void
	{

		activityCDInMS = aliments.length * 2000;

		currentState = Organe.STATE_MAX_AMPLITUDE;

		if (soundChannel1 != null) {
			soundChannel1.stop();
		}
		soundChannel1 = sound1.play(0, 2 * aliments.length);
		activate();
		super.contract();
	}

	override public function addAliment(a:Aliment)
	{
		a.arrivedCallback = null;

		super.addAliment(a);
		currentState += Math.round(Organe.STATE_MAX_AMPLITUDE / NB_MAX_ALIMENTS);
		
	}

}